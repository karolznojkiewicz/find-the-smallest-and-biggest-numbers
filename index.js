(function () {
    const minMax = (arr) => {
        let sorted = arr.sort((a, b) => a - b);

        let smaller = sorted[0];
        let largest = sorted[arr.length - 1];

        return smaller, largest;
    };
})();
